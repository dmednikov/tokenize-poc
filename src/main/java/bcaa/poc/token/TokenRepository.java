package bcaa.poc.token;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "token", path = "token")
public interface TokenRepository extends MongoRepository<Token, String>{
	public Token findById(String Id);
}