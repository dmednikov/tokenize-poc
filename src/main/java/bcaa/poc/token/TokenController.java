package bcaa.poc.token;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("/api")
public class TokenController {
	public static final Logger logger = LoggerFactory.getLogger(TokenController.class);
	
	@Autowired
	TokenService tokenService; //Service which will do all data retrieval/manipulation work

    @RequestMapping(value = "/token/up", method = RequestMethod.GET)
    public ServiceUp greeting() {
        return new ServiceUp();
    }
    
    
    // -------------------Retrieve Single Token------------------------------------------
  	@RequestMapping(value = "/token/{id}", method = RequestMethod.GET)
  	public ResponseEntity<?> fetchToken(@PathVariable("id") String id) {
  		logger.info("Fetching token with id {}", id);
  		Token token = tokenService.findById(id);
  		if (token == null) {
  			logger.error("Token with id {} not found.", id);
  			return new ResponseEntity(new CustomErrorType("Token with id " + id 
  					+ " not found"), HttpStatus.NOT_FOUND);
  		}
  		return new ResponseEntity<Token>(token, HttpStatus.OK);
  	}
  	
  	// -------------------Replace/Edit Single Token------------------------------------------
   	@RequestMapping(value = "/token/{id}", method = RequestMethod.PUT)
   	public ResponseEntity<?> replaceTokenValue(@PathVariable("id") String id, @RequestBody Token token) {
   		logger.info("Fetching token with id {}", id);
   		Token localToken = tokenService.findById(id);
   		if (localToken == null) {
   			logger.error("Token with id {} not found.", id);
   			return new ResponseEntity(new CustomErrorType("Token with id " + id 
   					+ " not found"), HttpStatus.NOT_FOUND);
   		}
   		localToken.setContent(token.getContent());
   		Token response = tokenService.replaceOne(localToken);
   		return new ResponseEntity<Token>(response, HttpStatus.OK);
   	}
  	
  	// -------------------Create a Token-------------------------------------------
  	@RequestMapping(value = "/token", method = RequestMethod.POST)
  	public ResponseEntity<?> createToken(@RequestBody Token token, UriComponentsBuilder ucBuilder) {
  		logger.info("Creating Token : {}", token);

  		if (tokenService.doesTokenExist(token)) {
  			logger.error("Unable to create. A Token with id {} already exist", token.getId());
  			return new ResponseEntity(new CustomErrorType("Unable to create. A Token with id " + 
  			token.getId() + " already exist."),HttpStatus.CONFLICT);
  		}
  		Token resultToken = tokenService.saveToken(token);
  		
  		final String template = "{\"id\": \"%s\"}";
  		HttpHeaders headers = new HttpHeaders();
  		headers.setContentType(MediaType.APPLICATION_JSON);
  		headers.setLocation(ucBuilder.path("/api/token/{id}").buildAndExpand(resultToken.getId()).toUri());
  		return new ResponseEntity<String>(String.format(template, resultToken.getId()), headers, HttpStatus.CREATED);
  	}
  	
  	// -------------------Retrieve All Tokens---------------------------------------------
   	@RequestMapping(value = "/tokens", method = RequestMethod.GET)
   	public ResponseEntity<List<Token>> listAllTokens() {
   		List<Token> tokens = tokenService.findAllTokens();
   		if (tokens.isEmpty()) {
   			logger.error("Calling /tokens on empty datastore.");
   			return new ResponseEntity(new CustomErrorType("The datastore is empty."), HttpStatus.NOT_FOUND);
   		}
   		return new ResponseEntity<List<Token>>(tokens, HttpStatus.OK);
   	}
   	
   	// -------------------Retrieve Token Count---------------------------------------------
   	@RequestMapping(value = "/tokens/count", method = RequestMethod.GET)
   	public ResponseEntity<?> getTokenCount() {
   		Long count = tokenService.getTokenCount();
   		final String template = "{\"count\": \"%s\"}";
   		HttpHeaders headers = new HttpHeaders();
   		headers.setContentType(MediaType.APPLICATION_JSON);
   		return new ResponseEntity<String>(String.format(template, count), headers, HttpStatus.OK);
   	}
  	
}
