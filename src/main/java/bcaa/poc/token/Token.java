package bcaa.poc.token;

import org.springframework.data.annotation.Id;

public class Token {
	
	@Id
    public String id;

    public String content;


    public Token(String content) {
        this.content = content;
    }
    
    public String getId(){
    	return this.id;
    }
    
    public void setId(String id){
    	this.id = id;
    }
    
    public String getContent(){
    	return this.content;
    }
    
    public void setContent(String content){
    	this.content = content;
    }

    @Override
    public String toString() {
        return String.format(
                "Token [id=%s, content='%s']",
                id, content);
    }
}