package bcaa.poc.token;

import java.util.List;

public interface TokenService {
	public Token findById(String Id);
    public Token saveToken(Token token);
    public void deleteById(String Id);
    public void deleteAll();
    public boolean doesTokenExist(Token token);
    public List<Token> findAllTokens();
    public Long getTokenCount();
    public Token replaceOne(Token token);
}
