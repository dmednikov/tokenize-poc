package bcaa.poc.token;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service("customerService")
public class TokenServiceImpl implements TokenService{
	
	private TokenRepository repository;
	
	public TokenServiceImpl(TokenRepository tokenRepository){
		this.repository = tokenRepository;
	}
	
	@Override
	public void deleteAll() {
		repository.deleteAll();
		
	}

	@Override
	public Token findById(String Id) {
		return repository.findById(Id);
	}

	@Override
	public Token saveToken(Token token) {
		Long count = repository.count();
		SecureRandom random = new SecureRandom();
		token.setId(new BigInteger(130, random).toString(32));
		return repository.save(token);
	}

	@Override
	public void deleteById(String Id) {
		repository.delete(Id);
	}

	@Override
	public boolean doesTokenExist(Token token) {
		return findById(token.getId())!=null;
	}

	@Override
	public List<Token> findAllTokens() {
		return repository.findAll();
	}

	@Override
	public Long getTokenCount() {
		return repository.count();
	}

	@Override
	public Token replaceOne(Token token) {	
		return repository.save(token);
	}

}
