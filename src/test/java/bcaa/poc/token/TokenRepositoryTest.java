package bcaa.poc.token;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import junit.framework.TestCase;

@ActiveProfiles({ "test" })
@RunWith(SpringRunner.class)
@SpringBootTest
public class TokenRepositoryTest extends TestCase{
	
	@Autowired
    TokenRepository repository;
	
	@Before
    public void initObjects() {
        repository.deleteAll();
    }

	@Test
    public void shouldSaveOneToken() {
		
		repository.save(new Token("whatever 1"));
		
		Long count = repository.count();

        assertThat(count).isNotNull();
        
        assertThat(count).isEqualTo(1);
    }
	
	@Test
    public void shouldSaveMoreThanOneToken() {
		
		repository.save(new Token("whatever 1"));
		repository.save(new Token("whatever 2"));
		repository.save(new Token("whatever 3"));
		
		Long count = repository.count();

        assertThat(count).isNotNull();
        
        assertThat(count).isEqualTo(3);
    }
	

}
