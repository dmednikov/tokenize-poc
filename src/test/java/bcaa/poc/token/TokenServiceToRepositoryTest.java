package bcaa.poc.token;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import junit.framework.Assert;

@ActiveProfiles({ "test" })
@RunWith(SpringRunner.class)
@SpringBootTest
public class TokenServiceToRepositoryTest {

	private TokenService tokenService;
	
	//@Mock – Creates mock instance of the field it annotates
	@Mock
	private TokenRepository tokenRepository;
	
	@Before
    public void initObjects() {
		MockitoAnnotations.initMocks(this);
		tokenService = new TokenServiceImpl(tokenRepository);
    }

	@Test
    public void getAllTokens_should_return_all_tokens() {
		List<Token> tokens = new ArrayList<Token>();
		tokens.add(new Token("123"));
		tokens.add(new Token("345"));
		tokens.add(new Token("567"));
		
		Mockito.when(tokenRepository.findAll()).thenReturn(tokens);
		
		//call method under test
		List<Token> resultTokens = tokenService.findAllTokens();
 
        assertThat(tokens).isEqualTo(resultTokens);
    }
	
	@Test
    public void saveToken_Should_return_valid_token_with_unique_id() {

    }
	
	@Test
    public void deleteToken_number_of_records_should_decrement_by_one_in_repository_after_delete() {

    }

}
