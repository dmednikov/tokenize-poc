# README #

This API is meant to help clients to eliminate the need for storing of sensitive data.
If one doesn't want to store customer's personal information in his own database
he can store a token from this API and retrieve the data on demand.

Similar to how payment gateways handle transactions with their tokenization 
services. The difference however is that payment gateway perform a transaction when you 
hand them a token and there is no data returned to client. 

From http://www.mastercard.com/gateway/payment-processing/tokenization.html:  
Merchants wishing to accept multiple payments from a customer's bank card, without the PCI DSS burden of requesting and retaining the card number for every transaction, have the option of submitting encrypted data (a 'token' or a 'reference') associated with the customer's card number or previously authorised transaction via MasterCard Payment Gateway Services Payment and Card Tokenization solutions. 

### Building on local machine ###

* Checkout this repo to your local drive  
`git clone https://dmednikov@bitbucket.org/dmednikov/tokenize-poc.git`
* Install Docker Toolbox and make sure you can start Docker console (well on Windows Docker Toolbox, .. whatever is needed to make Docker work on your machine)
* To build the project (from Docker console):  
`mvn clean package docker:build`
* To start Docker containers (from Docker console):  
`mvn docker:start`

### Deploying to Docker cloud and AWS ###

* Push Docker image to Docker hub (right now it has dmednikov prefix for my repository):  
`docker login`  
`docker push dmednikov/tokenize-poc`
* Login into https://cloud.docker.com
    * I already have a connection setup to AWS enabling Docker to deploy to my AWS EC2 instance
    * Docker's write-up on AWS setup is awesome but was missing a `PassRole` policy that I had to add to avoid permission problems
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "ec2:*",
                "iam:ListInstanceProfiles"
            ],
            "Effect": "Allow",
            "Resource": "*"
        },
        {
            "Action": [
                "iam:PassRole"
            ],
            "Effect": "Allow",
            "Resource": "*"
        }
    ]
}
```
* Create service from stock Mongo image (name it `mongo` !!!)
* Create service from tokenize-poc repository with a link to Mongo container
    * Setup port 80 on node to forwarded to 8080 on container
    * Setup link to mongo with alias `mongo`

### Technical

* Spring Boot - for API and Web development
    * Security
    * Data
    * io.fabric8 maven plugin (there are others: https://github.com/fabric8io/shootout-docker-maven)
* Mongo - for back end storage
* Docker - deployment pipeline
* AWS - deployment